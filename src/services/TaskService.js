import axios from "axios";

let instance = axios.create();

export function updateAxiosInstance() {
  instance.defaults.baseURL = 'https://api.jsonbin.io/v3/b/' + localStorage.getItem('JSON_BIN_ID');
  instance.defaults.headers.common['X-Master-key'] = localStorage.getItem('JSON_BIN_SECRET');
}

export async function getAll() {
  const res = await instance.get('/latest')
  return res.data.record
}

export async function updateAll(tasks) {
  const res = await instance.put('/', tasks)
  return res.data.record
}

updateAxiosInstance()