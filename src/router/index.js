import * as VueRouter from 'vue-router'

import HomePage from '../pages/Home.vue'

const SettingsPage = () => import('../pages/Settings.vue')
const LoginPage = () => import('../pages/Login.vue')
const NotFoundPage = () => import('../pages/NotFound.vue')
const SettingsApp = () => import('../components/SettingsApp.vue')
const SettingsUser = () => import('../components/SettingsUser.vue')

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes : [
    {
      path: '/',
      name: 'home',
      alias: '/home',
      component: HomePage,
      meta: { needJsonBin : true },
      children : [
        {
          path: '/home/:taskID',
          component: HomePage
        },
      ]
    },
    {
      path: '/settings',
      name: 'Settings',
      component: SettingsPage,
      meta: { needJsonBin : true },
      children : [
        {
          path: 'app',
          component: SettingsApp,
          meta: { needJsonBin : false },
        },
        {
          path: 'user',
          component: SettingsUser,
          meta: { needJsonBin : true },
        }
      ]
    },
    {
      path : '/not-found',
      name: 'NotFound',
      component: NotFoundPage
    },
    {
      path : '/:wrongPath(.*)',
      redirect: (to) => {
        return { name : 'NotFound', params : { wrongPath: to.params.wrongPath }}
      },
    },
  ]
})

// Mise en place de l'authentification pour chaque route
router.beforeEach((to, from) => {
  if(to.meta.needJsonBin && !localStorage.getItem('jsonBinAccess')) {
    return '/settings/app'
  }
})

export default router