import { v4 as uuid } from '@lukeed/uuid';

export default {
  namespaced: true,
  state() {
    return {
      history: [],
      notifier: null
    }
  },
  mutations:  {
    SET_NOTIFIER(state, value) {
      state.notifier = value
    },
    ADD_NOTIFICATION(state, newNotification) {
      state.history.push(newNotification)
    }
  },
  actions: {
    saveNotification({ commit }, element) {
      const id = uuid()
      commit('ADD_NOTIFICATION', {
        id: id,
        startTime: Date.now(),
        element
      })

      return id
    },

    /**
     * @param {*} { state, dispatch } 
     * @param {*} { type, title, message }  
     */
    notify({ state, dispatch }, options) {
       const element = state.notifier({
        offset : 50,
        duration: 3000,
        ... options
      })
      return dispatch('saveNotification', element)
    },
    warning({ dispatch }, options) {
      return dispatch('notify', { type: 'warning',  ... options })
    },
    success({ dispatch }, options) {
      return dispatch('notify', { type: 'success', ... options })
    },
    error({ dispatch }, options) {
      return dispatch('notify', { type: 'error',  ... options })
    }
  },
  getters: { 
    getNotificationByID: (state) => (id) => {
      return state.history.find(notification => notification.id === id)
    }
  }
}