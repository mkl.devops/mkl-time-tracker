import { v4 as uuid } from '@lukeed/uuid';
import *  as TaskService from '../../services/TaskService.js'

export default {
  namespaced: true,
  state () {
    return {
      tasks: null,
      areTasksLoading: false,
      currentTaskname: '',
      currentStartTime: null,
      isTaskInProgress : false
    }
  },
  mutations: {
    SET_TASKS(state, tasks) {
      state.tasks = tasks
    },
    ADD_TASK(state, newTask) {
      state.tasks.unshift(newTask)
    },
    DELETE_TASK(state, taskID) {
      state.tasks = state.tasks.filter(task => task.id !== taskID)
    },
    SET_ARE_TASKS_LOADING(state, bool){
      state.areTasksLoading = bool
    },
    SET_CURRENT_TASKNAME(state, taskname) {
      state.currentTaskname = taskname
    },
    SET_CURRENT_START_TIME(state, startTime) {
      state.currentStartTime = startTime
    },
    SET_IS_TASK_IN_PROGRESS(state, bool) {
      state.isTaskInProgress = bool
    },
  },
  actions: {
    async fetchAllTasks({commit}) {
      commit('SET_ARE_TASKS_LOADING', true)
      try {
        const tasks = await TaskService.getAll()
        commit('SET_TASKS', tasks)
        commit('SET_ARE_TASKS_LOADING', false)
      } catch(error) {
        commit('SET_ARE_TASKS_LOADING', false)
        throw error
      }
    },
    async updateAllTasks({ state }) {
      await TaskService.updateAll(state.tasks)
    },
    deleteTask({ commit }, taskID) {
      commit('DELETE_TASK', taskID)
    },
    startTask({ commit }) {
      commit('SET_IS_TASK_IN_PROGRESS', true)
      commit('SET_CURRENT_START_TIME', Date.now())
    },
    stopTask({ state, commit }) {
      const newTask = {
        id : uuid(),
        name: state.currentTaskname,
        startTime: state.currentStartTime,
        endTime : Date.now()
      }
      commit('ADD_TASK', newTask)
      commit('SET_IS_TASK_IN_PROGRESS', false)
      commit('SET_CURRENT_START_TIME', null)
      commit('SET_CURRENT_TASKNAME', '')
    },
    restartTask({ state, commit, dispatch, getters }, taskID) {
      if(state.isTaskInProgress) {
        dispatch('stopTask')
      }

      const task = getters.getTaskByID(taskID)
      
      setTimeout(() => {
        commit('SET_CURRENT_TASKNAME', task.name)
        dispatch('startTask')
      })
    }
  },
  getters: {
    getTaskByID: (state) => (id) => {
      return state.tasks.find(task => task.id === id)
    },
    getTaskIndexByID: (state) => (id) => {
      return state.tasks.findIndex(task => task.id === id)
    },
    tasksByDay(state) {
      if(state.tasks) {
        const tasksByDay = {}
        state.tasks.forEach(task => {
          const currentDayTs = (new Date(task.startTime)).setHours(0,0,0,0)
          if(!tasksByDay[currentDayTs])
            tasksByDay[currentDayTs] = []

          tasksByDay[currentDayTs].push(task)
        });
        return tasksByDay
      } else {
        return {}
      }
    }
  }
}