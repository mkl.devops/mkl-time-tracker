import { createStore, createLogger } from 'vuex'
import TasksModule from './modules/task.js'
import NotificationsModule from './modules/notifications'

const store = createStore({
  modules: {
    tasks : TasksModule,
    notifications : NotificationsModule,
  },
  plugins: import.meta.env.MODE !== 'production' ? [createLogger()] : []
})

export default store